﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AzureCI.Models;

namespace AzureCI.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            ShowTheThingModel Model = new ShowTheThingModel()
            {
                ShowTheThing = (Request.Method == "POST"),
            };

            return View(Model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
